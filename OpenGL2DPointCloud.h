#pragma once

#include <vector>
#include "Common/Types.h"
#include "Visible/Visible.h"
#include "Color/Color.h"
#include "Messenger/Listener.h"

class CWindow;

class COpenGL2DPointCloud : public CVisible, public CListener
{
public:
							COpenGL2DPointCloud() = delete;
							COpenGL2DPointCloud( CColor oColor );
							~COpenGL2DPointCloud() override;
	void					Add( f32 fX, f32 fY ) { m_vPoints.push_back( std::pair< f32, f32 >(fX, fY) ); }
	void					Draw() const override;
	size_t					GetNumPoints() const { return m_vPoints.size(); }
	std::pair< f32, f32 >	GetPoint( u32 i ) const { return m_vPoints[ i ]; }
	void					Push( CMessage& ) override;

private:
	CColor									m_oColor;
	std::vector< std::pair< f32, f32 > >	m_vPoints;
};
