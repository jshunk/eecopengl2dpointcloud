#include "OpenGL2DPointCloud.h"
#include "windows.h"
#include <gl\gl.h>
#include "Common/Utilities.h"
#include "Window/Window.h"
#include "Messenger/Messenger.h"

COpenGL2DPointCloud::COpenGL2DPointCloud( CColor oColor )
	: CVisible()
	, m_oColor( oColor )
{
	assert( CWindow::GetDefaultWindow() );
	CMessenger::GlobalListen( *this, CWindow::GetDefaultWindow()->GetDraw2DMessage() );
}

COpenGL2DPointCloud::~COpenGL2DPointCloud()
{
	assert( CWindow::GetDefaultWindow() );
	assert( CMessenger::GlobalIsListening( *this, CWindow::GetDefaultWindow()->GetDraw2DMessage() ) );
	CMessenger::GlobalStopListening( *this, CWindow::GetDefaultWindow()->GetDraw2DMessage() );
}

void COpenGL2DPointCloud::Draw() const
{
	f32 fWindowHalfWidth = CWindow::GetDefaultWindow()->GetVirtualWidth() / 2;
	f32 fWindowHalfHeight = CWindow::GetDefaultWindow()->GetVirtualHeight() / 2;

	glColor3f( m_oColor.R(), m_oColor.G(), m_oColor.B() );
	glBegin( GL_POINTS );
		for( std::pair< f32, f32 > oPoint : m_vPoints )
		{
			// Convert -half width to half width to -1 to 1:
			f32 fX( NUtilities::ConvertCoordinateNoBounds( oPoint.first, -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f ) );
			f32 fY( NUtilities::ConvertCoordinateNoBounds( oPoint.second, -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f ) );
			glVertex2f( fX, fY );
		}
	glEnd();
}

void COpenGL2DPointCloud::Push( CMessage& )
{
	Draw();
}
